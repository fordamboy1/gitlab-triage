require 'spec_helper'

require 'gitlab/triage/command_builders/label_command_builder'

describe Gitlab::Triage::CommandBuilders::LabelCommandBuilder do
  it_behaves_like 'label command', 'label'
end
